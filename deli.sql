-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.5-10.1.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2017-08-28 07:00:46
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for deli
DROP DATABASE IF EXISTS `deli`;
CREATE DATABASE IF NOT EXISTS `deli` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `deli`;


-- Dumping structure for table deli.complain
DROP TABLE IF EXISTS `complain`;
CREATE TABLE IF NOT EXISTS `complain` (
  `complain_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `complain_invoice` varchar(50) DEFAULT NULL,
  `complain_no` varchar(50) DEFAULT NULL,
  `complain_date` date DEFAULT NULL,
  `complain_name` varchar(50) DEFAULT NULL,
  `complain_address` varchar(150) DEFAULT NULL,
  `complain_phone` varchar(20) DEFAULT NULL,
  `complain_desc` text,
  PRIMARY KEY (`complain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
