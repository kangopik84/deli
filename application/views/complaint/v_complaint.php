<?php

defined('BASEPATH') OR exit('No direct script access allowed');  ?>

<!DOCTYPE html>
<html lang="en">
	<head>
        <?php $this->load->view('includes/v_head'); ?>
    </head>
    <body>
		<!-- Page container -->
		<div class="page-container">
			<!-- Page content -->
			<div class="page-content">
				<!-- Main content -->
				<div class="content-wrapper">
					<div class="panel panel-flat">
						<div class="panel-body">
							<form action="<?php echo base_url('home/InsertComplain'); ?>" method="post" id="formMain" class="form-horizontal form-label-left">
								<fieldset class="content-group">									
									<div class="form-group">
										<a href="#" class="control-label col-lg-1">
			    							<span class="avatar-xs avatar pull-left mt-5 mr10" >
		                					<img alt="..." src="<?php echo base_url()."assets/images/deli.png"; ?>">
			                			</span></a>
			                			<label class="control-label col-lg-8" style="padding-top: 20px; font-size: 25px; font-style: italic;">Complain Form</label>
									</div>									
									<?php echo $this->session->flashdata('notif') ?>
									<div class="form-group">
										<label class="control-label col-lg-8"></label>
										<label class="control-label col-lg-1">Inv No</label>
										<label class="control-label col-lg-1">:</label>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-8"></label>
										<label class="control-label col-lg-1">Comp ID</label>
										<label class="control-label col-lg-1">: TMF-CF/</label>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-8"></label>
										<label class="control-label col-lg-1">Tarikh</label>
										<label class="control-label col-lg-1" style="width: 1px; padding-right: 0;">:</label>
										<div class="col-lg-2">
											<input type="text" class="form-control" name="complain_date" id="complain_date" readonly="readonly">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-1">Name</label>
										<div class="col-lg-3">
											<input type="text" class="form-control" name="complain_name" id="complain_name" required="required">
											<span class="help-block"></span>
										</div>
									</div>	
									<div class="form-group">
										<label class="control-label col-lg-1">Address</label>
										<div class="col-lg-10">
											<input type="text" class="form-control" name="complain_address" id="complain_address" required="required">
											<span class="help-block"></span>
										</div>
									</div>	
									<div class="form-group">
										<label class="control-label col-lg-1">Phone</label>
										<div class="col-lg-2">
											<input type="text" class="form-control" name="complain_phone" id="complain_phone" required="required" onkeypress='return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46 || event.which == 37 || event.which == 39'>
											<span class="help-block"></span>
										</div>
									</div>	
									<div class="form-group">
										<label class="control-label col-lg-1">Complain</label>
										<div class="col-lg-10">
											<textarea name="complain_desc" cols="40" rows="5" id="complain_desc" class="form-control" required="required"></textarea>
											<span class="help-block"></span>
										</div>
									</div>	
								</fieldset>
								<div class="col-lg-11">
									<div class="text-right">
										<button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
										<button type="button" class="btn btn-danger" id="btnClear" onclick="clearcomplain()">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
    
    <script type="text/javascript">
    var today = new Date();
    var yrs = today.getFullYear();
    var month = today.getMonth()+1;
    var day = today.getDate();
    var output = (day<10 ? '0' : '') + day + '-' + (month<10 ? '0' : '') + month + '-' + yrs;

    $(document).ready(function() {
    	$('#complain_date').val(output);
    });

    function clearcomplain()
    {
    	$('#complain_name').val('');
    	$('#complain_address').val('');
    	$('#complain_phone').val('');
    	$('#complain_desc').val('');
    }
    
    $(window).bind("load", function() {
	  	  window.setTimeout(function() {
	  	    $(".alert").fadeTo(500, 0).slideUp(500, function(){
	  	        $(this).remove();
	  	    });
	  	}, 4000);
	  });
	</script>
</html>