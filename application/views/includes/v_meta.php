<?php

defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" >
<meta name="viewport" content="width=device-width, initial-scale=1">
<link  rel="shortcut icon" type="image/x-icon" href="<?php echo config_item('images'); ?>deli.ico" />
<title><?php echo config_item('web_title'); ?></title>