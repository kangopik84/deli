<?php

defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('includes/v_meta'); ?>

<!-- css -->
<link href="<?php echo config_item('icons'); ?>icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo config_item('bootstrap'); ?>css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo config_item('dist'); ?>css/core.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo config_item('dist'); ?>css/components.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo config_item('dist'); ?>css/colors.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo config_item('plugins'); ?>alertify/themes/alertify.core.css" rel="stylesheet">
<link href="<?php echo config_item('plugins'); ?>alertify/themes/alertify.default.css" id="toggleCSS" rel="stylesheet">
<link href="<?php echo config_item('dist'); ?>css/style.css" rel="stylesheet" type="text/css">

<!-- js -->
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>loaders/pace.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('bootstrap'); ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('bootstrap'); ?>js/bootstrap-datetimepicker.min.js"></script>		
<script type="text/javascript" src="<?php echo config_item('bootstrap'); ?>js/bootstrap-datetimepicker.id.js"></script>
<script type="text/javascript" src="<?php echo config_item('bootstrap'); ?>js/bootstrap-timepicker.min.js"></script>	
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>loaders/blockui.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>ui/nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>ui/drilldown.js"></script>

<!-- Theme JS files -->
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>ui/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>pickers/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>forms/selects/select2.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>notifications/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>notifications/sweet_alert.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('dist'); ?>pages/components_modals.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>alertify/lib/alertify.min.js"></script>
<script type="text/javascript" src="<?php echo config_item('plugins'); ?>jquery/jquery.mask.js"></script>