<?php

defined('BASEPATH') OR exit('No direct script access allowed');  ?>

<!DOCTYPE html>
<html lang="en">
	<head>
        <?php $this->load->view('includes/v_head'); ?>
    </head>
    <body>
    	<!-- Page container -->
		<div class="page-container">
			<!-- Page content -->
			<div class="page-content">
				<!-- Main content -->
				<div class="content-wrapper">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="form-horizontal form-label-left">
								<div class="form-group">
									<a href="#" class="control-label col-lg-1">
		    							<span class="avatar-xs avatar pull-left mt-5 mr10" >
	                					<img alt="..." src="<?php echo base_url()."assets/images/deli.png"; ?>">
		                			</span></a>
		                			<label class="control-label col-lg-8" style="padding-top: 20px; font-size: 25px; font-style: italic;">Complain List</label>
								</div>
							</div>
							<?php echo $this->session->flashdata('notif') ?>
							<table id="table" class="table table-striped table-bordered table-hover" style="cellspacing:0; width:100%;">
								<thead>
									<tr>
										<th width=5% class="text-center">No</th>
										<th class="text-center">Date</th>
										<th class="text-center">Name</th>
										<th class="text-center">Address</th>
										<th class="text-center">Phone</th>
										<th class="text-center">Complain</th>
										<th width=5% class="text-center">Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
    
    <script type="text/javascript">
    var table;
    $('document').ready(function(){
    	table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('admin/complain_list')?>",
                "type": "POST",
                "data": {"<?php echo $this->security->get_csrf_token_name(); ?>":"<?php echo $this->security->get_csrf_hash(); ?>"}
            },       
            "columnDefs": [{
                    "targets": [ -1, 0], 
                    "orderable": false, 
            }],
            "dom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            "language": {
            	"search": '_INPUT_',
                "lengthMenu": '_MENU_ per page',
                "paginate": { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' },
                "infoEmpty": "",
            	"infoFiltered":"",
            	"emptyTable":"No record found.",
            	"zeroRecords":"No record found.",
            	"info":"Showing _START_ to _END_ of _TOTAL_ entries",
    	    },      
    	    "lengthChange": false,
    	    "pageLength": 10,      
    	    "searching": false,
        });

    	// Alternative pagination
        $('.datatable-pagination').DataTable({
            pagingType: "simple",
            language: {
                paginate: {'next': 'Selanjutnya &rarr;', 'previous': '&larr; Sebelumnya'}
            }
        });

        // Datatable with saving state
        $('.datatable-save-state').DataTable({
            stateSave: true
        });

        // Scrollable datatable
        $('.datatable-scroll-y').DataTable({
            autoWidth: true,
            scrollY: 300
        });

        // Add placeholder to the datatable filter option
        $('.dataTables_filter input[type=search]').attr('placeholder','Ketik Pencarian...');


        // Enable Select2 select for the length option
        $('.dataTables_length select').select2({
            minimumResultsForSearch: "-1"
        });

    	$("input").change(function(){
    		$(this).parent().parent().removeClass('has-error');
    		$(this).next().empty();
    		});
    		
    	$("select").change(function(){
    		$(this).parent().parent().removeClass('has-error');
    		$(this).next().empty();
    	});
    });

    function deletes(id)
    {
        var mySplitResult = id.split("/");

        swal({
        	  title: "Confirmation",
        	  text: "Are you sure to delete this data "+mySplitResult[1]+" ?",
        	  showCancelButton: true,
        	  confirmButtonColor: "#DD6B55",
        	  confirmButtonText: "Yes",
        	  cancelButtonText: "No",
        	  closeOnConfirm: true,
        	  closeOnCancel: true,
        	},
        	function(isConfirm){
        	  if (isConfirm) {
        		  $.ajax({
        	            url : "<?php echo site_url('admin/deletecomplain')?>/"+mySplitResult[0],
        	            type: "POST",
        	            dataType: "JSON",
        	            success: function(data){
    						if(data.status == 0){ window.location.href = data.url_home; }
    						if(data.status == 1){
    							$('#ResponseInput').html(data.pesan);
    						}
    					}
        	        });
        	  }
        });
    }

    $(window).bind("load", function() {
	  	  window.setTimeout(function() {
	  	    $(".alert").fadeTo(500, 0).slideUp(500, function(){
	  	        $(this).remove();
	  	    });
	  	}, 4000);
	  });
	</script>
</html>