<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_complain_table extends CI_Migration {
    public function __construct(){
        $this->load->dbforge();
    }
    public function up()
    {
        $this->dbforge->add_field(array(
            'complain_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
            ),
            'complain_invoice' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50, 
            		'null' => TRUE,
            ),
            'complain_no' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50, 
            		'null' => TRUE,
            ),
            'complain_date' => array(
                    'type' => 'DATE',
            		'null' => TRUE,
            ),
            'complain_name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50, 
            		'null' => TRUE,
            ),
            'complain_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 150, 
            		'null' => TRUE,
            ),
            'complain_phone' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 20, 
            		'null' => TRUE,
            ),
            'complain_desc' => array(
                    'type' => 'TEXT',
                     'null' => TRUE,
            ),
        ));
        $this->dbforge->add_key('complain_id', TRUE);
        $this->dbforge->create_table('complain');
    }

    public function down()
    {
        $this->dbforge->drop_table('complain');
    }
}