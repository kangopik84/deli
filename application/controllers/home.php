<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
	
	var $table = 'complain';
	var $column = array('complain_date', 'complain_name','complain_address','complain_phone','complain_desc');
	var $order = array('complain_date' => 'asc');
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->view('complaint/v_complaint');
	}
	
	public function InsertComplain()
	{
		$now = new DateTime ( NULL, new DateTimeZone('Asia/Jakarta'));
		
		$data = array(
				'complain_invoice'    	=> '',
				'complain_no'    		=> '',
				'complain_date'        	=> $now->format('Y-m-d H:i:s'),
				'complain_name'			=> $this->input->post('complain_name'),
				'complain_address'		=> $this->input->post('complain_address'),
				'complain_phone'		=> $this->input->post('complain_phone'),
				'complain_desc'			=> $this->input->post('complain_desc'),
		);
		
		$insert = $this->M_crud->save($this->table, $data);
		if($insert > 0)
		{
			$this->session->set_flashdata("notif",
					"<div class=\"alert bg-success alert-styled-left\">
							<span class=\"text-semibold\">Complain created successfully.</span>
						</div>");
			redirect("Home");
		}
		else
		{
			$this->session->set_flashdata("notif",
					"<div class=\"alert bg-warning alert-styled-left\">
							<span class=\"text-semibold\">Complain failed !</span>, please try again.
						</div>");
			redirect("Home");
		}
	}
	
}