<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {
	
	var $table = 'complain';
	var $column = array('complain_id','complain_date', 'complain_name','complain_address','complain_phone','complain_desc');
	var $order = array('complain_date' => 'asc');
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->view('admin/v_admin');
	}

	public function complain_list()
	{
		$list = $this->M_view->get_datatables($this->table, $this->column, $this->order);
		
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $datas) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = date('d-m-Y', strtotime($datas->complain_date));
			$row[] = $datas->complain_name;
			$row[] = $datas->complain_address;
			$row[] = $datas->complain_phone;
			$row[] = $datas->complain_desc;
		
			$rowDelete = '<a href="javascript:void(0);" title="Hapus" onclick="deletes('."'".$datas->complain_id."/".$datas->complain_name."'".')" ><i class="icon-trash"></i></a>';
			$row[] = $rowDelete;
		
			$data[] = $row;
		}
		
		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->M_view->count_all($this->table, $this->order),
					"recordsFiltered" => $this->M_view->count_filtered($this->table, $this->column, $this->order),
					"data" => $data,
			);
		
		echo json_encode($output);
	}
	
	public function deletecomplain($id)
	{
		$delete = $this->M_crud->delete_by_id1($this->table, "complain_id", $id);
		if($delete){		
			$this->session->set_flashdata("notif",
					"<div class=\"alert bg-success alert-styled-left\">
								<span class=\"text-semibold\">Delete complain successfully.</span>
							</div>");
			$URL_home = site_url('admin');						
			$json['status']		= 0;
			$json['url_home'] 	= $URL_home;
			echo json_encode($json);
		}else{
			$pesan = "Delete complain failed !, please try again.";
			$json['status'] = 1;
			$json['pesan'] 	= "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>".$pesan."</div>";
			echo json_encode($json);
		}
	}
	
}