<?php
/**
 * author @Taufik Surachman
 * copyright @2016
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class M_view extends CI_Model {
	
private function _get_datatables_query($table, $column, $order, $key = NULL, $where1 = NULL, $where2 = NULL)
	{
		if($key == NULL){
			$this->db->from($table);
		}
	
		$i = 0;
			
		foreach ($column as $item)
		{
			if($_POST['search']['value'])
			{
					
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
	
				if(count($column) - 1 == $i)
					$this->db->group_end();
			}
			$column[$i] = $item;
			$i++;
		}
			
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if(isset($order))
		{
			$order = $order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	
	function get_datatables($table, $column, $order, $key = NULL, $where1 = NULL, $where2 = NULL)
	{
		$this->_get_datatables_query($table, $column, $order, $key, $where1, $where2);
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		
		$query = $this->db->get();
		return $query->result();
	}
	
	function count_filtered($table, $column, $order, $key = NULL, $where1 = NULL, $where2 = NULL)
	{
		$this->_get_datatables_query($table, $column, $order, $key, $where1, $where2);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function count_all($table, $param1 = NULL, $where1 = NULL, $param2 = NULL, $where2 = NULL, $param3 = NULL, $where3 = NULL)
	{
		$this->db->from($table);
		if (!is_null($param1))
			$this->db->where($param1, $where1);
			if (!is_null($param2))
				$this->db->where($param2, $where2);
				if (!is_null($param3))
					$this->db->where($param3, $where3);
		return $this->db->count_all_results();
	}
}
