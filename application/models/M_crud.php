<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model {
	
	public function save($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}
	
	public function delete_by_id1($table, $where, $id, $where1 = NULL, $id1 = NULL, $where2 = NULL, $id2 = NULL)
	{
		$this->db->where($where, $id);
		if (!is_null($where1))
			$this->db->where($where1, $id1);
			if (!is_null($where2))
				$this->db->where($where2, $id2);
	
				$this->db->delete($table);
				if($this->db->affected_rows()){
					return true;
				}
				else{
					return false;
				}
	}
	
}